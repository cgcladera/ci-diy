import { CiDiyPage } from './app.po';

describe('ci-diy App', function() {
  let page: CiDiyPage;

  beforeEach(() => {
    page = new CiDiyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
